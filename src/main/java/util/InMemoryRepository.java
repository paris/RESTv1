package util;

import customer.Customer;
import customer.CustomerId;
import token.Token;
import token.TokenId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryRepository implements Repository {
    private List<Customer> customers;
    private List<Token> tokens;

    public InMemoryRepository() {
        customers = new ArrayList<>();
        tokens = new ArrayList<>();

        Customer customer = new Customer(new CustomerId("customer1"));
        customers.add(customer);
        Token token = new Token(new TokenId("token1"), false);
        Token token2 = new Token(new TokenId("token2"), false);
        tokens.add(token);
        tokens.add(token2);
    }

    public boolean containsCustomer(CustomerId customerId) {
        return customers.stream().anyMatch(c -> c.getId().equals(customerId));
    }

    public boolean containsToken(TokenId tokenId) {
        return tokens.stream().anyMatch(t -> t.getId().equals(tokenId));
    }

    public void createCustomer(Customer customer) {
        customers.add(customer);
    }

    public void updateCustomer(Customer customer) {
        customers.stream().filter(c -> c.equals(customer)).collect(Collectors.toList());
    }

    public void updateToken(Token token) {
        tokens.stream().filter(t -> t.equals(token)).collect(Collectors.toList());
    }

    public Optional<Customer> getCustomer(CustomerId customerId) {
        return customers.stream().filter(c -> c.getId().equals(customerId)).findFirst();
    }

    public Optional<Token> getToken(TokenId tokenId) {
        return tokens.stream().filter(t -> t.getId().equals(tokenId)).findFirst();
    }

    public List<Token> getTokens() {
        return tokens;
    }
}
