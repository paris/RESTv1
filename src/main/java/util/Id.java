package util;

public abstract class Id {
    private String id;

    public Id() {}

    public Id(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        /*
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }


        return id.equals(((Id) obj).getId());
        */
        return true;
    }

    public int hashCode() {

        return 0;
    }

    public String getId() {
        return id;
    }
}
