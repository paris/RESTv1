package token;

public class Token {
    private TokenId id;
    private boolean used;

    public Token(TokenId id, boolean used) {
        this.id = id;
        this.used = used;
    }

    public boolean isUsed() {
        return used;
    }

    public void use() {
        used = true;
    }

    public TokenId getId() {
        return id;
    }
}
