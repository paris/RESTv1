package customer;

import token.Token;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private CustomerId id;
    private List<Token> tokens = new ArrayList<>();

    public Customer() {}

    public Customer(CustomerId id) {
        this.id = id;
    }

    public Customer(Customer customer) {
        this.id = customer.getId();
    }

    public CustomerId getId() {
        return id;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void addTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
