package rest;

import customer.Customer;
import customer.CustomerId;
import token.Token;
import token.TokenId;
import token.TokenService;
import util.InMemoryRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/tokens")
public class TokensResource {
    public static TokenService tokenService = new TokenService(new InMemoryRepository());

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<Token> requestTokens(Customer customer, @QueryParam("number") int number) {
        try {
            return tokenService.requestTokens(customer, number);
        } catch (Exception e) {
            throw new BadRequestException(Response
                        .status(Response.Status.BAD_REQUEST)
                        .entity(e.getMessage())
                        .build());
        }
    }

    @GET
    @Produces("application/json")
    public List<Token> getTokens() {
        return tokenService.getTokens();
    }

    /*
    * ONLY FOR TESTING - REMOVE
    * */
    @Path("/customer/{customerId}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Optional<Customer> getCustomer(@PathParam("customerId") String customerId) {
        try {
            return TokensResource.tokenService.getCustomer(new CustomerId(customerId));
        } catch (Exception e) {
            throw new BadRequestException(Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build());
        }
    }
}
