package rest;

import token.Token;
import token.TokenId;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/tokens/{tokenId}")
public class TokenResource {

    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Optional<Token> getToken(@PathParam("tokenId") String tokenId) {
        try {
            return TokensResource.tokenService.getToken(new TokenId(tokenId));
        } catch (Exception e) {
            throw new BadRequestException(Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build());
        }
    }

    @POST
    @Consumes("application/json")
    public void useToken(@PathParam("tokenId") String tokenId) {
        try {
            TokensResource.tokenService.useToken(new TokenId(tokenId));
        } catch (Exception e) {
            throw new BadRequestException(Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build());
        }
    }
}
